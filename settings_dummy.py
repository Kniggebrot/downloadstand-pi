raspberry = True                                # Are you executing this script on a Raspberry Pi?
pin       = 17                                        # Output pin for raspberry
use_ssl   = { "SMTP"  : True,                     # Use SSL or STARTTLS for secure communication with the SMTP or IMAP server?
              "IMAP"  : True }                    # Configure as needed
time      = 3600                                     # Time to wait until next cycle in seconds; can be lowered to 1800, as the downloaded amount will only be refreshed every 30 mins