#!/usr/bin/env python3
"""
Ein Skript, das eine Mail an den Downloadstand-Service des Studierendenwerks sendet,
um daraus den aktuellen Downloadstand zu berechnen und als Spannung an einem Pin (sowie der Konsole) auszugeben.

"""
import keyring
from globals import service,creds,backend
new_creds = True
new_settings = True
# Initial setup: Detect previous configuration (if available) and create encrypted version of user data
try:
    from settings import raspberry, pin, use_ssl, time
except:
    new_settings = False

try:
    from get_creds import user, passwd, from_mail, to_mail, smtp, imap, port_smtp, port_imap
    for i in creds:
        if eval(i) == None:
            new_creds = False
            break
except:
    new_creds = False

if not new_settings:
    print("No settings.py detected.\nMany user settings have been moved to settings.py in the last update; please configure setttings_dummy.py to your liking and save it as settings.py.")
    print("For further information on the settings in settings_dummy.py please read the README.")
    raise OSError("Missing settings.py")

if raspberry == True:
    keyring.set_keyring(eval(backend))

if not new_creds:
    try:                            # Check for creds.py
        from creds import user, passwd, from_mail, to_mail, smtp, imap, port_smtp, port_imap
    except:
        print("No creds.py found.")
        print("User credentials, mails and servers have been moved to creds.py, from where they will be imported, encrypted and stored in a separate file.")
        print("Please configure creds_dummy.py as needed and save it as creds.py.")
        print("For further information on the settings in creds_dummy.py please read the README.")
        raise OSError("Missing creds.py")
    else:
        print("Exporting credentials to OS keyring...")
        for i in creds:
            keyring.set_password(service,i,eval(i) )
        print("Credentials exported. \"creds.py\" can be deleted now.")
        input("Press Enter to continue...")




# User settings for SSL and "PC mode", decryption key
if not new_settings:
    from settings import raspberry, pin, use_ssl, time

# LED = Voltage output
if raspberry == True:
    from gpiozero import PWMLED
from time import sleep

import sys
import socket

# Mail
import ssl, smtplib
import imaplib
from email.message import EmailMessage

# Login data & mail addresses
if not new_creds:
    from get_creds import user, passwd, from_mail, to_mail, smtp, imap, port_smtp, port_imap

# Sent mail:
with open("Downloadstand.txt") as fp:           # Read Downloadstand.txt to set the content of the mail
    send = EmailMessage()                       # The content of the mail, doesnt't matter, but the subject.
    send.set_content(fp.read())                 # So feel free to write whatever you want in Downloadstand.txt
    
send['Subject'] = 'Downloadstand'
send['From'] = from_mail
send['To'] = to_mail

# Setup
if raspberry == True:
    voltage = PWMLED(pin)                        # (Raspberry Pi) Want output on another port? Change here

# SSL
_DEFAULT_CIPHERS = (
'ECDH+AESGCM:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:ECDH+HIGH:'
'DH+HIGH:ECDH+3DES:DH+3DES:RSA+AESGCM:RSA+AES:RSA+HIGH:RSA+3DES:!aNULL:'
'!eNULL:!MD5')
# To ensure a secure connection for Python <= 3.5.1, a default context isn't secure. See https://stackoverflow.com/questions/33857698/sending-email-from-python-using-starttls
if sys.version_info.minor <=5 and sys.version_info.micro <=1:
    context = ssl.SSLContext(ssl.PROTOCOL_TLS)  # (If this doesn't work, try ssl.PROTOCOL_SSLv23 as an alias for TLS)
    context.options |= ssl.OP_NO_SSLv2          # Disabling old and unsecure protocols SSL v2 and v3
    context.options |= ssl.OP_NO_SSLv3

    context.set_ciphers(_DEFAULT_CIPHERS)       # Ensure proper ciphers are used
    context.set_default_verify_paths()          # Load trusted system certificates
    context.verify_mode = ssl.CERT_REQUIRED     # IMPORTANT: Verify server certificates are trusted
else:
    context = ssl.create_default_context()

#########################################################################################################################################################################


print("Willkommen im Downloadstand-Programm!")
try:
    handle2=open("Stand.dat","r")                              # Read last volume percentage from "Stand.dat". Generate example if missing
    if handle2.mode == "r":
        ds_percent = handle2.read()
    handle2.close()
except FileNotFoundError:
    print("Stand.dat nicht gefunden! Generiere Beispiel...")
    handle2 = open("Stand.dat","w")
    handle2.write("0.9")
    handle2.close()
    ds_percent = "0.9"

if raspberry == True:
    voltage.value = float(ds_percent.strip())                  # (Raspberry Pi) Output last volume percentage

# Main Loop
while True:
    # Exception handling: All mail operations are encapsulated in loops which will only be exited if the operation was successful. --> *_err set to 0
    smtp_err = 1        # Once all operations have been completed, the variables need to be reset to an unsuccessful state, which happens here, at the start of the loop.
    imapr_err = 1       # The amount of current tries is also tracked with these.
    imapd_err = 1

    while smtp_err != 0:
        try:
            print("Sende Anfrage an Downloadstand-Server...")
            # Mail:
            if use_ssl["SMTP"] == True:                                                         # Using dictionary to determine which connection method to use
                with smtplib.SMTP_SSL(smtp,port=port_smtp,context=context) as server_send:      # Code will only be executed if ssl.wrap_socket() is successful
#                   server_send.set_debuglevel(1) # for debug messages, make sure the indentation is correct!
                    server_send.login(user,passwd)
                    server_send.send_message(send)
                print("Anfrage gesendet! Warte auf Antwort...")
                smtp_err = 0
                sleep(180)
            else:
                with smtplib.SMTP(smtp,port=port_smtp) as server_send:
#                   server_send.set_debuglevel(1) # for debug messages, make sure the indentation is correct!
                    if server_send.starttls(context=context)[0] != 220:                         # Ensure STARTTLS succeded --> Response code is "220"
                        print("Fehler bei TLS!")
                        quit()
                    server_send.login(user,passwd)
                    server_send.send_message(send)
                print("Anfrage gesendet! Warte auf Antwort...")
                smtp_err = 0
                sleep(180)                                      # (below) Did a recoverable exception happen? Retry after 30 s (for an infinite amount of time, until successful)
        except (socket.gaierror, smtplib.SMTPServerDisconnected, smtplib.SMTPResponseException, smtplib.SMTPDataError, smtplib.SMTPConnectError, smtplib.SMTPHeloError, smtplib.SMTPNotSupportedError, smtplib.SMTPAuthenticationError):
            print(f"Fehler beim Senden der Mail. Versuche es in 30 Sekunden wieder... (Versuch {smtp_err})")
            smtp_err += 1
            sleep(30)
        except:                                                 # For exceptions not relating to connection issues
            print(f"Unerwarteter Fehler: {sys.exc_info()[0]}")
            raise

    while imapr_err != 0:
        try:
            print("Empfange Antwort:")
            if use_ssl["IMAP"] == True:
                with imaplib.IMAP4_SSL(imap, port=port_imap, ssl_context=context) as server_rec:       # Exception if unsuccessful
#                   server_rec.debug = 99 # for debug messages, make sure the indentation is correct!
                    server_rec.login(user,passwd)
                    server_rec.select("INBOX/Downloadstand")
                    msg = server_rec.fetch("2", "(BODY[TEXT])")
                    if msg[0] == 'NO':
                        raise AssertionError("No second mail received")  # A failed fetch does not raise an error on its own. Thus the assertion error; OSError could be raised by something unrelated (could also construct own exception class)
                print("Antwort empfangen!")
                imapr_err = 0
            else:
                with imaplib.IMAP4(imap, port=port_imap) as server_rec: 
#                   server_rec.debug = 99 # for debug messages, make sure the indentation is correct!
                    server_rec.starttls(ssl_context=context)
                    if server_rec._tls_established != True:     # Ensure socket is wrapped
                        print("Fehler bei TLS!")
                        quit()
                    server_rec.login(user,passwd)
                    server_rec.select("INBOX/Downloadstand")
                    msg = server_rec.fetch("2", "(BODY[TEXT])")
                    if msg[0] == 'NO':
                        raise AssertionError("No second mail received")  # A failed fetch does not raise an error on its own. Thus the assertion error
                print("Antwort empfangen!")
                imapr_err = 0
        except (AssertionError, socket.gaierror, imaplib.IMAP4.abort, imaplib.IMAP4.readonly):  # recoverable IMAP errors
            if imapr_err <= 5:
                print(f"Fehler beim Empfangen der Mail. Versuche es in 30 Sekunden wieder... (Versuch {imapr_err})")
                imapr_err += 1
                sleep(30)
            else:
                print("Mail nach 5 Versuchen immer noch nicht empfangen. Beginne von vorn...")
                smtp_err = 0      # To skip the rest of the loop, set all exception variables to 0
                imapr_err = 0
                imapd_err = 0
        except:                                                 # For exceptions not relating to connection issues
            print(f"Unerwarteter Fehler: {sys.exc_info()[0]}")
            raise

    while imapd_err != 0:
        try:
            if use_ssl["IMAP"] == True:
                with imaplib.IMAP4_SSL(imap, port=port_imap, ssl_context=context) as server_rec:
#                   server_rec.debug = 99 # for debug messages, make sure the indentation is correct!
                    server_rec.login(user,passwd)
                    server_rec.select("INBOX/Downloadstand")
                    server_rec.store("1", '+FLAGS', '\\Deleted')
                imapd_err = 0
            else:
                with imaplib.IMAP4(imap, port=port_imap) as server_rec:
#                  server_rec.debug = 99 # for debug messages, make sure the indentation is correct!
                    server_rec.starttls(ssl_context=context)
                    if server_rec._tls_established != True:
                        print("Fehler bei TLS!")
                        quit()
                    server_rec.login(user,passwd)
                    server_rec.select("INBOX/Downloadstand")
                    server_rec.store("1", '+FLAGS', '\\Deleted')
                imapd_err = 0
        except (socket.gaierror, imaplib.IMAP4.abort, imaplib.IMAP4.readonly):
            print(f"Fehler beim Löschen der alten Mail. Versuche es in 30 Sekunden wieder... (Versuch {imapd_err})")
            imapd_err += 1
            sleep(30)
        except:                                                 # For exceptions not relating to connection issues
            print(f"Unerwarteter Fehler: {sys.exc_info()[0]}")
            raise
        else:
            msg_slice=str(msg).split()                                          # Mail processing: Split the mail into its words (+ response code)
            ds = int(msg_slice[25])                                             # Read the amount of downloaded data, put it in an integer
            ds_percent = round(ds / 500000, 4)                                  # Generate a percentage with 4 significant digits
            if ds_percent > 1:                                                  # Sanity check: If more data than allowed has been downloaded,
                ds_percent = 1                                                  # percentage will still be set to 100 %, for the voltage output (could be moved after print())
            print(f"Der Downloadstand beträgt: {ds} MB ({ds_percent*100} %)")   # Output amount and percentage to stdout
    
            handle3 = open("Stand.dat","w")                                     # Save percentage in Stand.dat
            if handle3.mode == "w":
                handle3.write(str(ds_percent))
            handle3.close()
    
            # GPIO:
            if raspberry == True:                                               # (Raspberry Pi) Output percentage on selected pin
                voltage.value = ds_percent
    
            sleep(time)                                                         # Wait time in seconds. The amount will only be refreshed every 30 min; can be set to greater values (in seconds) if needed (to avoid spam)
    
print("That's all folks?!") # to close all remaining open loops / code blocks

