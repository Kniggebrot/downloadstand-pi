#!/usr/bin/env python3
import keyring
from globals import service, creds

print("Clearing creds from keyring...")

for i in creds:
    keyring.delete_password(service,i)

print("All creds cleared.")