user      = "mail@mail.com"  # Do not remove the quotation marks!
passwd    = "secret"
from_mail = "mail@mail.com"                  # Please change to your mail address
to_mail   = "downloadstand@mail.de"             # Please change to the real downloadstand mail adress
smtp      = "smtp.mail.com"   # Configure to your SMTP server
port_smtp = 587

imap      = "imap.mail.com"   # Configure to your IMAP server
port_imap = 993