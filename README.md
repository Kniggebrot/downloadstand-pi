# Downloadstand (Raspberry Pi)

Residents of the Studierendenwerk Hamburg can access the amount of their used data volume by sending a mail to the downloadstand service of the Studierendenwerk Hamburg. This Python script automates this process by sending a mail every 30 minutes, extracting the downloaded amount of data from the response and, if running the [master](https://gitlab.com/Kniggebrot/downloadstand_pi/-/tree/master) branch on a Raspberry Pi, outputting the percentage of used data volume as a PWM signal on pin GPIO17.

## Recommended inbox setup

This script is designed to look for a "Downloadstand" subfolder of the inbox, so it will not interfere with regular mails in the inbox. All received mails containing *"Downloadstand"* in their subject should be moved to this folder; a rule doing this can and should be created at your mail provider.

## Required modules

To properly use this script, you need to install `keyring`, which is used to store the credentials as safely as possible in the OS keyring. If using Linux, you may need to install `secretstorage` and `dbus-python` as a possible backend for `keyring` as well.

For usage of the voltage output, you need `GPIOZero`, which should be preinstalled on your Raspberry Pi.

## Configurable options

To make sure the script works as intended, you have to configure a few values:

### Raspberry / PC mode:

In "settings.py", you can change `raspberry` from `True` to `False` to be able to execute this script on a standard PC. By doing this, the interpreter will not import GPIOZero and configure the (nonexistent) pin for PWM; you can also use this variable to toggle the voltage output. An example settings.py is provided as settings_dummy.py.

### GPIO pin

In "settings.py", you can specify the output pin using `pin` in line 2.

An integer will be referenced to the corresponding GPIO pin; the GPIO number of a pin can be found out by running `pinout` in a terminal on the Pi. There are many more options to select the pin in a way you wish; they can be found in the documentation of [GPIOZero](https://gpiozero.readthedocs.io/en/stable/recipes.html).

### Sender mail

The mail address used to send the mails needs to be edited to yours in creds.py, line 3 (`from_mail`).

Authentication details need to be provided in this file as well, with username as `user` in line 1 and password as `pass` in line 2. An example configuration is provided as "creds_dummy.py".
As this script doesn't support 2FA, you may need to generate an app password for this script.

### Downloadstand mail

In line 4, you have to change `to_mail` to the real downloadstand address.

### SSL / STARTTLS

To configure the usage of SSL (True) or STARTTLS (False) for your mail servers, modify the values of lines 3f in "settings.py" to your liking:
```python
use_ssl = { "SMTP"  : True,
            "IMAP"  : True }
```

### SMTP and IMAP server

The SMTP and IMAP server need to be configured in "creds.py", lines 5ff:
```python
smtp      = "smtp.mail.com"
port_smtp = 587

imap      = "imap.mail.com"
port_imap = 993
```

These settings depend on your mail provider and should be found there.

### Waiting time

The waiting time until the next Downloadstand request can be changed in line 5 of settings, in the variable `time`. Any values lower than 1800 are not recommended, as the amount of downloaded data will only be refreshed every 30 minutes and the sending of a reply with said amount in it may take up to 180 seconds.

## Clear credentials

If you want to clear your credentials from your OS keyring, you can use `python clear_creds.py` from this directory.

## Contact

If you encounter any problems while running the script, please post an issue in the issue tracker [here](https://gitlab.com/Kniggebrot/downloadstand-pi/-/issues).

## Used libraries:

  - [keyring](https://pypi.org/project/keyring/)
  - [GPIOZero](https://gpiozero.readthedocs.io/en/stable/index.html)
  - [SMTPLib](https://docs.python.org/3/library/smtplib.html)
  - [IMAPLib](https://docs.python.org/3/library/imaplib.html)
  - [email](https://docs.python.org/3/library/email.html)
  - [ssl](https://docs.python.org/3/library/ssl.html)
  - [time](https://docs.python.org/3/library/time.html)

