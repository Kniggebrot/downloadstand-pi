import keyring
from globals import service

user      = keyring.get_password(service,"user")
passwd    = keyring.get_password(service,"passwd")
from_mail = keyring.get_password(service,"from_mail")
to_mail   = keyring.get_password(service,"to_mail")
smtp      = keyring.get_password(service,"smtp")
imap      = keyring.get_password(service,"imap")
port_smtp = keyring.get_password(service,"port_smtp")
port_imap = keyring.get_password(service,"port_imap")